# Article To Docx

El objetivo de este programa es poder crear documentos en formato Docx a partir de artículos de diferentes sitios y blogs. Se ingresa uno o mas links de algún articulo y se obtiene un documento docx listo para ser editado o para imprimir.



## Uso basico

Lo primero que hay que hacer es instalar todas las dependencias necesarias.

#### _instalar dependencias:_

`pip install -r requirements.txt`

#### _iniciar el programa:_

`python main.py`

Se da la opcion de obtener un solo documento o varios, en cualquiera de los dos casos se pedira el link correspondiente del articulo que quisieramos obtener. ( Por el momento solo funciona con articulos de [**Dev.to**](https://dev.to/) )

Como resultado se crearan dos carpetas nuevas, Documents y Zip documents, en documents es donde encontraremos los documentos creados...

## Dependencias

- beautifulsoup4
- python-docx
- requests

## Por hacer
#### Sitio que me gustaria agregar:
- Wikipedia
- Real python
- Medium
- hashnode

#### Funcionalidades que estoy trabajando:
- La posibilidad de exportar todos los documentos comprimidos en Zip

#### A futuro
- Tener una interfaz grafica.
