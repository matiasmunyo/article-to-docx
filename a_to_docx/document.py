# DOCX DOCUMENTATION --> https://python-docx.readthedocs.io/
from docx import Document
from docx.shared import Inches

from .article import Article


class Docx:
    def __init__(self, data, filename = 'New-document'):
        self.data = data
        self.filename = filename
        
        self.doc = Document()
        
        self.__format()
        
        
    def __format(self):
        print('Generando el documento')
        self.doc.add_heading(self.data.title, level=0)
        for i in self.data.content['body']:
            if i[:3] == '[2]':
                self.doc.add_heading(i[3:], level=1)
            elif i[:3] == '[3]':
                self.doc.add_heading(i[3:], level=2)
            elif i[:3] == '[4]':
                self.doc.add_heading(i[3:], level=3)
            elif i[:3] == '[p]':
                self.doc.add_paragraph(i[3:])
            elif i[:3] == '[*]':
                self.doc.add_paragraph(i[3:], style='List Bullet')
            elif i[:3] == '[-]':
                self.doc.add_paragraph(i[3:], style='List Number')
            elif i[:3] == '[c]':
                code = self.doc.add_paragraph(i[3:])
                code.paragraph_format.left_indent = Inches(.5)
                code.style = 'macro'
        
        
    def export(self, folder):
        self.doc.save('{}\\{}.docx'.format(folder, self.filename))
        print(f'( {self.data.title} ) generado correctamente!')