from .article import Article
from .document import Docx
from .files_sys import FileSystem


class Controller:
    def __init__(self):
        self.url_input = None
        self.user_url = None
        self.urls_list = []
        
        self.fs = FileSystem('Documents', 'Zip-documents')
        
    def main(self):
        print('Deseas guardar un articulo o varios?')
        print('Ingrese "1" para un solo articulo\nIngrese "2" para varios articulos')
        self.user_input = input("--> ")
        if self.user_input == "1":
            print('Ingrese la URL del articulo...')
            self.user_url = input('url --> ')
            self.simple_url()
        elif self.user_input == "2":
            self.multiple_url()
        else:
            print('Opcion no valida')
            self.main()
        
        
    def simple_url(self):
        self.new_article = Article(self.user_url)
        self.new_document = Docx(self.new_article, self.new_article.title[:10]).export(self.fs.docs_path)
                        
    
    def multiple_url(self):
        while True:
            print('Ingrese la URL del articulo...')           
            self.url_input = input('url: ')
            self.urls_list.append(self.url_input)
            print('Desea agregar guardar otro articulo?\n"1" para agregar otro\n"2" para guardar los articulos ingresados')
            self.user_input = input("--> ")
            if self.user_input == '1':
                continue
            elif self.user_input == '2':
                break            
        
        # INSTANTIATION OF ARTICLES AND DOCUMENTS
        self.articles = [Article(url) for url in self.urls_list]
        self.documents = [Docx(article, article.title).export(self.fs.docs_path) for article in self.articles]