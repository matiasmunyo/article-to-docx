# https://realpython.com/working-with-files-in-python/
import os
import zipfile


class FileSystem:
    def __init__(self, docs_name, zip_name):
        self.basepath = os.getcwd()
        self.docs_path = docs_name
        self.zip_path = zip_name
        
        self.__create_folders()
        
        
    def __create_folders(self):
        try:
            os.mkdir(self.docs_path)
            os.mkdir(self.zip_path)
            print('Carpetas para los documentos y zips creadas correctamente!')
        except Exception as e:
            print(f'Error: {e}')
    
    
    def create_zip(self):
        pass