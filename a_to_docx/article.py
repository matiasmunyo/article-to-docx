import requests
from bs4 import BeautifulSoup


class Article():
    def __init__(self, url):
        self.url = url
        
        self.title = 'Article from Dev.to'
        self.content = {
            "title" : self.title,
            "author" : 'Desconocido',
            "body" : self.__get_content()
        }
        
    
    def __find_content(self):
        self.web = requests.get(self.url).text
        self.soup = BeautifulSoup(self.web, 'html.parser')
        
        self.title = self.soup.find(id='main-title').h1.text.strip()
        self.article_content = self.soup.find(id='article-body')
        
        return self.article_content
    
    def __get_content(self):
        self.clear_content = []
        for tag in self.__find_content():
            if tag.name == 'h2':
                self.clear_content.append(f'[2]{tag.text.strip()}')
            elif tag.name == 'h3':
                self.clear_content.append(f'[3]{tag.text.strip()}')          
            elif tag.name == 'h4':
                self.clear_content.append(f'[4]{tag.text.strip()}')          
            elif tag.name == 'p':
                self.clear_content.append(f'[p]{tag.text.strip()}')             
            elif tag.name == 'ul':
                for i in tag:
                    if i.name == 'li':
                        self.clear_content.append(f'[*]{i.text.strip()}')
            elif tag.name == 'ol':
                for i in tag:
                    if i.name == 'li':
                        self.clear_content.append(f'[-]{i.text.strip()}')
            elif tag.name == 'div':
                if 'highlight' in tag['class']:
                    self.clear_content.append(f'[c]{tag.code.text.strip()}')
                                                       
        return self.clear_content

    
    def to_text(self, file_name):
        with open(f'{file_name}.txt', 'w') as txt:
            for i in self.content["body"]:
                try:
                    txt.writelines(i+'\n')
                except UnicodeEncodeError:
                    print('Hay textos que posiblemente se perdieran por contener Emojis...')
                    continue